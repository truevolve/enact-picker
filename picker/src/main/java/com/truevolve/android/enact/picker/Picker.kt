package com.truevolve.android.enact.picker

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException

import org.json.JSONException
import org.json.JSONObject

class Picker : ActivityBaseController(), View.OnClickListener {

    private var selection: Int = 0
    private var rangeMax: Int = 0
    private var rangeMin: Int = 0
    private var selectionIndication: TextView? = null
    private var buttonAdd: Button? = null
    private var buttonSubtract: Button? = null

    override val type: String
        get() = TYPE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        try {

            val displayMessage = findViewById<View>(R.id.displayMessage) as TextView
            displayMessage.text = stateObj?.getString(DISPLAY_MESSAGE)

            rangeMax = stateObj?.getInt(RANGE_MAX) ?: 0
            rangeMin = stateObj?.getInt(RANGE_MIN) ?: 100

            selection = rangeMin

            selectionIndication = findViewById<View>(R.id.selectionIndication) as TextView
            selectionIndication?.text = selection.toString()

            val nextButton = findViewById<View>(R.id.buttonNext) as Button
            if (stateObj?.has(NEXT_BUTTON_TEXT) == true) {
                val nextButtonText = stateObj?.getString(NEXT_BUTTON_TEXT)
                nextButton.text = nextButtonText
            }

            buttonAdd = findViewById<View>(R.id.buttonAdd) as Button
            buttonAdd?.setOnClickListener(this)
            buttonSubtract = findViewById<View>(R.id.buttonSubtract) as Button
            buttonSubtract?.setOnClickListener(this)

            nextButton.setOnClickListener {
                try {

                    val pickerSelection = PickerSelection(selection)
                    stateObj?.getString(STORE_AS)?.let { Interpreter.dataStore.put(it, pickerSelection) }

                    stateObj?.getString(ON_NEXT)?.let { goToState(it) }

                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: InterpreterException) {
                    e.printStackTrace()
                }
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        Log.d(TAG, "validate: starting")

        if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: state object must have $STORE_AS defined")
            throw PolicyException("State object must have $STORE_AS defined")
        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $DISPLAY_MESSAGE defined")
            throw PolicyException("State object must have $DISPLAY_MESSAGE defined")
        } else if (!stateObj.has(RANGE_MIN)) {
            Log.e(TAG, "validate: state object must have $RANGE_MIN defined")
            throw PolicyException("State object must have $RANGE_MIN defined")
        } else if (!stateObj.has(RANGE_MAX)) {
            Log.e(TAG, "validate: state object must have $RANGE_MAX defined")
            throw PolicyException("State object must have $RANGE_MAX defined")
        } else if (!stateObj.has(ON_NEXT)) {
            Log.e(TAG, "validate: state object must have $ON_NEXT defined")
            throw PolicyException("State object must have $ON_NEXT defined")
        }
    }

    override fun onClick(v: View) {

        val i = v.id
        if (i == R.id.buttonAdd) {

            if (selection < rangeMax) {
                selection++
                buttonAdd?.setOnClickListener(this)
            } else if (selection == rangeMax) {
                buttonSubtract?.setOnClickListener(this)
            } else {
                buttonAdd?.setOnClickListener(null)
            }

        } else if (i == R.id.buttonSubtract) {

            if (selection > rangeMin) {
                selection--
                buttonSubtract?.setOnClickListener(this)
            } else if (selection == rangeMin) {
                buttonAdd?.setOnClickListener(this)
            } else {
                buttonSubtract?.setOnClickListener(null)
            }
        }

        selectionIndication?.text = selection.toString()
    }

    companion object {

        private val TAG = Picker::class.java.simpleName
        private val STORE_AS = "store_as"
        private val ON_NEXT = "on_next"
        private val DISPLAY_MESSAGE = "display_message"
        private val RANGE_MIN = "range_min"
        private val RANGE_MAX = "range_max"
        private val NEXT_BUTTON_TEXT = "next_button_text" //optional

        private val TYPE = "picker"
    }
}
