package com.truevolve.android.enact.picker;

/**
 * Created by developer on 3/28/17.
 */

public class PickerSelection {

    public int selection;
    private String type = "number_picker";

    public PickerSelection(){}

    public PickerSelection(int selection){
        this.selection = selection;
    }

    public int getSelection() {
        return selection;
    }

    public String getType() {
        return type;
    }
}
