package com.truevolve.android.enact.picker.sample;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

public class Menu extends ActivityBaseController {

    private static final String ON_START = "on_start";
    private static final String TAG = "Menu";
    private static final String TYPE = "menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        try {
            final String onStart = getStateObj().getString(ON_START);

            findViewById(R.id.buttonStart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        goToState(onStart);
                    } catch (InterpreterException | JSONException e) {
                        e.printStackTrace();
                        Interpreter.INSTANCE.error(Menu.this, e);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        if (!stateObj.has(ON_START)) {
            Log.e(TAG, "validate: state object does not have " + ON_START + " which is mandatory");
            throw new PolicyException("State object does not have " + ON_START + " which is mandatory");
        }
    }
}
