package com.truevolve.android.enact.picker.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.truevolve.android.enact.picker.Picker;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String policy =
            "{\n" +
                    "  \"start\": {\n" +
                    "    \"on_start\": \"picking\",\n" +
                    "    \"type\": \"menu\"\n" +
                    "  },\n" +
                    "  \"picking\": {\n" +
                    "    \"type\": \"picker\",\n" +
                    "    \"store_as\": \"picker_count\",\n" +
                    "    \"on_next\": \"summary\",\n" +
                    "    \"display_message\": \"Use the buttons to pick a number\",\n" +
                    "    \"range_min\":1,\n" +
                    "    \"range_max\":10,\n" +
                    "    \"next_button_text\":\"Submit\"\n" +
                    "  },\n" +
                    "  \"summary\": {\n" +
                    "    \"retrieve_as_selection\": \"picker_count\",\n" +
                    "    \"display_message\": \"You picked the number %s.\",\n" +
                    "    \"on_done\": \"end\",\n" +
                    "    \"type\": \"summary\"\n" +
                    "  }\n" +
                    "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getIntent().hasExtra(Interpreter.END)){
            Log.d(TAG, "onCreate: ENDED");
        }
        if(getIntent().hasExtra(Interpreter.ERROR)){
            Log.d(TAG, "onCreate: ERRORED");
            Throwable throwable = (Throwable) getIntent().getSerializableExtra("exception");
            Log.e(TAG, "onCreate: Errored: ", throwable);
        }

        List<Class<? extends ActivityBaseController>> listOfControllers = new ArrayList<>();
        listOfControllers.add(Menu.class);
        listOfControllers.add(Picker.class);
        listOfControllers.add(Summary.class);

        try {
            Interpreter interpreter = Interpreter.INSTANCE.setup(MainActivity.class, policy, listOfControllers);
            if(interpreter != null){
                interpreter.start(this);
            }

        } catch (JSONException | PolicyException | InterpreterException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG, "onResume: RESUMING");
    }
}
